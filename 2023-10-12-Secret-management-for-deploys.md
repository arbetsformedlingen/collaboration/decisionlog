### Title
Using External Secrets Operator with Bitwarden Backend for Secret
Management and Deploys

### Context
Our organization does not currently have a proper way to manage and
deploy secrets to the Openshift clusters. We have selected External
Secrets Operator (ESO) with Bitwarden (BW) as backend for this purpose, as we
believe this will solve the problem, as well as give other positive
side effects - Bitwarden can also be used by the teams to communicate
secrets between developers in a safe way.

#### External Secrets Operator

External Secrets Operator is a Kubernetes operator that integrates
external secret management systems like Bitwarden. The operator reads
information from external APIs and automatically injects the values
into a Kubernetes Secret.

#### Bitwarden

Bitwarden is a freemium open-source password management service that
stores sensitive information such as website credentials in an
encrypted vault.


### Decision
We will implement a technical solution using External Secrets Operator
with Bitwarden Backend, with the goal of finishing the task before
2023-12-12. Two months is estimated to be sufficient for this quite
complex task.


### Status
Since around 2023-01-01, our team has tried to solve this problem
using a number of techniques, including Hashicorp Vault (failed
because of licensing problems), Amazon KMS (inconclusive results),
Sealed Secrets (initial investigations). However, we think that ESO +
BW shows the greatest potential, especially since it's provides useful
features for other areas as well.

Not formally decided yet. **Request for comments from teams.**


### Consequences
Positive consequences:
 - A proper secret management for cluster deploys will finally come in place.
 - BW can also be used to communicate secrets between developers in a safe way.
 - Fully installable onprem, so we will own both data and services.
 - Open source solution.
 - Widely used and maintained projects.
 - Simple architecture, easy to explain and use.

Negative consequences:
 - The legality of the data management is unclear in the context of
   our public governmental agency.
 - Higher workload for us to maintain our own service, compared to a cloud provider.

In conclusion, we believe the positives outweigh the negatives,
especially when compared to the other alternatives we have evaluated.
