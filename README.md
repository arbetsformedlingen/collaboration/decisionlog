# Decisionlog

This repository is used by the development team to document all their decisions in a markdown file and check it into this repository. Each decision should contain the following headers: "Title", "Context", "Decision", "Status", and "Consequences".

## Log
The table below can be rebuilt with `./dlogtool.py rebuild`.

| Date | Team | Title |
|---|---|---|
| 2023-03-28 |  | [Decisions logged to files in a repo.](2023-03-28-start-logging.md) |
| 2023-03-29 |  | [Publish Daily Operations Metrics](2023-03-29-Publish-Daily-Operations-Metrics.md) |
| 2023-03-29 |  | [Automatic Vulnerability Detection for GitLab Repositories](2023-03-29-Automatic-Vulnerability-Detection-for-GitLab-Repositories.md) |
| 2023-03-30 |  | [Allowing Anonymous Access to Open Data](2023-03-30-Allowing-Anonymous-Access-to-Open-Data.md) |
| 2023-03-30 |  | [Preventing Sensitive Information from Being Committed to Source Control](2023-03-30-Preventing-Sensitive-Information-from-Being-Committed-to-Source-Control.md) |
| 2023-03-31 |  | [Ensuring Reproducibility of Published Software](2023-03-31-Ensuring-Reproducibility-of-Published-Software.md) |
| 2023-08-09 |  | [Enhancing Security and Standardizing User Interface Development with Static Web Applications](2023-08-09-Static-Webapps.md) |
| 2023-10-12 |  | [Using External Secrets Operator with Bitwarden Backend for Secret Management and Deploys](2023-10-12-Secret-management-for-deploys.md) |
| 2023-10-12 |  | [Password Management Tool](2023-10-12-Password-Management-Tool.md) |
| 2024-04-25 | taxonomy-dev | [Taxonomy delivered as a .Net library](2024-04-25-Taxonomy-delivered-as-a-dotNet-library.md) |
| 2025-03-06 | OpenData | [Migration of JobAdLinks Storage to AWS S3](2025-03-06-Migration-of-JobAdLinks-Storage-to-AWS-S3.md) |

## How to document a decision

To document a decision, call `./dlogtool.py --team "TEAMNAME" new "TITLE"` replacing `TEAMNAME` by the team name and `TITLE` by the title of the decision. This will generate a new file for the decision and update the [Log](#log) table. Edit the file, save it, and commit it.

*Example:*
```
$ ./dlogtool.py --team "Calamari" new "New credentials"
2024-04-25-New-credentials.md
$ emacs 2024-04-25-New-credentials.md
$ git add 2024-04-25-New-credentials.md 
$ git commit -a -m "wip"
```

You can also manually create a new entry based on [`template.md`](template.md).

## Contributing
To contribute to the decision log, please fork this repository and create a new branch for your changes. Once your changes are complete, create a pull request back to the main branch. All contributions are welcome!

## License
This repository is licensed under the MIT license. See the LICENSE file for details.
