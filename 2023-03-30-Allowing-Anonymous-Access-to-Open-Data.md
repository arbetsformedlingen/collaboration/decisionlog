## Title
Allowing Anonymous Access to Open Data

## Context
Our organization provides access to open data through various delivery mechanisms, such as open APIs and data portals. We want to ensure that anyone can access this data without having to identify themselves or provide personal information.

## Decision
We will implement a policy that requires all open data delivery mechanisms to allow anonymous access. This means that users will not be required to identify themselves or provide personal information in order to access open data. The policy will apply to all new data delivery mechanisms, as well as any existing mechanisms that are being updated or modified.

## Status
Decided. We are continues reviewing our existing data delivery mechanisms to ensure that they allow anonymous access, and we are updating them as necessary. We are also working with our development teams to ensure that any new mechanisms that are developed are designed to allow anonymous access.

## Consequences
The policy will help to promote open access to information and increase transparency. It will also help to ensure that our organization is in compliance with relevant data protection and privacy laws. However, there may be some limitations to what data can be provided anonymously, such as data that is subject to licensing agreements or restrictions. We will work to identify these limitations and provide as much data as possible while still complying with legal requirements. 

Another consequence of allowing anonymous access is that it may be more difficult to track usage and provide customer support. Without identifying information, it may be more challenging to understand how users are interacting with the data and to address any issues that arise. However, we believe that the benefits of allowing anonymous access outweigh this potential drawback, and we will work to implement other mechanisms for monitoring usage and providing support where possible.
