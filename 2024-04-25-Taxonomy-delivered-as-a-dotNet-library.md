# Title
Taxonomy delivered as a .Net library

# Team
taxonomy-dev

# Context

Currently, the primary way of delivering the taxonomy is either by a REST API or a GraphQL API. It is also delivered in the form of downloadable files. Other software within AF is frequently calls the APIs of the taxonomy in order to stay up-to-date. There are multiple problems with this approach:

* If the API is temporarily not working, other software depending on the API may also not work properly.
* The performance of the API has the be sufficient to serve the incoming traffic.

We rarely release more often than once a month but other software keeps asking for data that has not changed since the last time it asked.

# Decision

We have made the decision to deliver the taxonomy in the form of software libraries for the .Net platform which is a platform widely used within AF. Delivering the taxonomy as a .Net library using Nuget hooks into an existing package management system. The library should be specific to our user in mind and easy to use. Only the subset of the taxonomy exposed by the library will be included. Providing the taxonomy as downloadable json files does not offer those two benefits.

# Status

**Decided.**

There is currently a .Net library in use that implements the functionality to map between ESCO ids and concept ids:

<https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtechtaxonomydotnet6>

# Consequences

## Benefits

* Less traffic to our API if some consumers decide to use the library instead of calling the API.
* More reliable software within AF that can work even if the taxonomy API would not be working.

## Risks

* We spend time implementing specific functionalities in the library that is not used in practice by any user.
* The release of a .Net library may cause confusion during the taxonomy adoption within AF.
* Potentially slower propagation of bug fixes, because it requires the user of the library to update a dependency.
