#! /usr/bin/env python3

import unittest
import re
from pathlib import Path
import os
import argparse
import sys
import datetime

log_entry_filename_pattern = re.compile("(\d\d\d\d)-?(\d\d)-?(\d\d)-(.*)\.[mM][dD]")

def parse_filename(filename):
    result = log_entry_filename_pattern.match(filename)
    if result is not None:
        return {"year": result.group(1),
                "month": result.group(2),
                "day": result.group(3),
                "title": result.group(4).replace("-", " ")}

markdown_header_pattern = re.compile("(#+)\s+(\S.*)")
    
def parse_markdown_line(s):
    header = markdown_header_pattern.match(s)
    if header is not None:
        return {"type": "section",
                "level": len(header.group(1)),
                "title": header.group(2)}
    return {"type": "line",
            "value": s}

def render_markdown(tree):
    stack = [x for x in reversed(tree)]
    output = []
    while 0 < len(stack):
        top = stack[-1]
        stack = stack[0:-1]
        ttype = top["type"]
        if ttype == "section":
            output.append("#"*top["level"] + " " + top["title"])
            stack.extend(reversed(top["children"]))
        elif ttype == "line":
            output.append(top["value"])
    return output
    
def parse_markdown_lines(lines):
    stack = [{"type": "section", "level": 0, "children": []}]
    current_level = 1

    def close_to_level(target_level):
        nonlocal stack
        while True:
            last = stack[-1]
            if last["level"] < target_level:
                break
            stack = stack[0:-1]
            stack[-1]["children"].append(last)
    
    for line in lines:
        pline = parse_markdown_line(line)
        ptype = pline["type"]
        if ptype == "section":
            close_to_level(pline["level"])
            pline["children"] = []
            stack.append(pline)
            current_level = 1 + pline["level"]
        elif ptype == "line":
            stack[-1]["children"].append(pline)
    close_to_level(1)
    return stack[0]["children"]

def postwalk_markdown_tree(elements, path, visitor):
    new_elements = []
    for x in elements:
        if x["type"] == "section":
            x = {**x, "children": postwalk_markdown_tree(x["children"],
                                                         path + [x["title"]],
                                                         visitor)}
        new_elements.append(visitor(path, x))
    return new_elements

def edit_section(elements, path_pred, visitor):
    def editor(path, element):
        if element["type"] == "section":
            inner_path = path + [element["title"]]
            if path_pred(inner_path):
                return visitor(element)
        return element
    return postwalk_markdown_tree(elements, [], editor)

def section_map(elements):
    dst = {}
    def visitor(path, element):
        if element["type"] == "section":
            dst[tuple(path + [element["title"]])] = element
        return element
    postwalk_markdown_tree(elements, [], visitor)
    return dst

def read_markdown(filename):
    with open(filename, "r") as f:
        return parse_markdown_lines(f.read().splitlines())

def write_markdown(filename, data):
    with open(filename, "w") as f:
        for line in render_markdown(data):
            f.write(line + "\n")

def render_section_lines(section):
    lines = [x for x in section["children"]
             if x["type"] == "line"]
    return "\n".join(render_markdown(lines)).strip()

def wrap_line(line):
    return {"type": "line", "value": line}

def get_section_string(smap, k):
    section = smap.get(k)
    return render_section_lines(section) if section is not None else None

def by_last_header(h):
    return lambda path: 0 < len(path) and path[-1].lower() == h.lower()

def with_new_children(children):
    return lambda section: {**section, "children": children}

def rebuild_repo(path):
    path = Path(path)
    entries = []
    for filename in os.listdir(path):
        parsed = parse_filename(filename)
        if parsed is not None:
            data = section_map(read_markdown(filename))
            title = get_section_string(data, ("Title",)) or parsed["title"]
            title = title.replace("\n", " ")
            team = get_section_string(data, ("Team",)) or ""
            year = parsed["year"]
            month = parsed["month"]
            day = parsed["day"]
            entries.append({"title": title,
                            "team": team,
                            "date": f"{year}-{month}-{day}",
                            "filename": filename})
    entries.sort(key=lambda x: x["date"])

    table_header = ["The table below can be rebuilt with `./dlogtool.py rebuild`.",
                    "",
                    "| Date | Team | Title |",
                    "|---|---|---|"]
    table_body = [f"| {date} | {team} | [{title}]({filename}) |"
                  for entry in entries
                  for date in [entry["date"]]
                  for title in [entry["title"]]
                  for filename in [entry["filename"]]
                  for team in [entry["team"]]]
    whitespace = [""]
    children = [wrap_line(line)
                for lines in [table_header, table_body, whitespace]
                for line in lines]

    readme_path = path.joinpath("README.md")
    old_readme = read_markdown(readme_path)
    new_readme = edit_section(
        old_readme,
        by_last_header("Log"),
        with_new_children(children))
    write_markdown(path.joinpath("README.md"), new_readme)

translation = str.maketrans({".": "dot",
                             " ": "-"})

def generate_filename(path, title):
    t = datetime.datetime.now()
    mangled_title = title.translate(translation)
    counter = 0
    while True:
        fname = "{:04d}-{:02d}-{:02d}-{:s}{:s}.md".format(
            t.year, t.month, t.day, mangled_title,
            "" if counter == 0 else str(counter))
        full_path = path.joinpath(fname)
        if not(full_path.exists()):
            return full_path
        counter += 1

def new_entry(path, title, team):
    full_path = generate_filename(path, title)
    entry = read_markdown(path.joinpath("template.md"))
    entry = edit_section(entry,
                         by_last_header("title"),
                         with_new_children([wrap_line(line)
                                             for line in [title, ""]]))
    entry = edit_section(entry,
                         by_last_header("team"),
                         with_new_children([wrap_line(line)
                                             for line in [team, ""]]))
    write_markdown(full_path, entry)
    return full_path
    

class TestDlogTool(unittest.TestCase):
    def test_parse_filename(self):
        self.assertEqual({'year': '2024',
                          'month': '03',
                          'day': '16',
                          'title': 'New decision'},
                         parse_filename("2024-03-16-New-decision.md"))
        self.assertEqual({'year': '2024',
                          'month': '03',
                          'day': '16',
                          'title': 'New decision'},
                         parse_filename("20240316-New-decision.md"))
        self.assertIs(None, parse_filename("New-decision.md"))

    def test_parse_markdown_line(self):
        self.assertEqual({'type': 'section', 'level': 2, 'title': 'Tjena'},
                         parse_markdown_line("## Tjena"))
        self.assertEqual({'type': 'line', 'value': 'Tjena'},
                         parse_markdown_line("Tjena"))

    def test_parse_markdown_lines(self):
        self.assertEqual([{'type': 'line', 'value': 'asdf'}],
                         parse_markdown_lines(["asdf"]))
        self.assertEqual([{'type': 'line', 'value': 'asdf'},
                          {'type': 'line', 'value': 'xyz'}],
                         parse_markdown_lines(["asdf", "xyz"]))
        self.assertEqual([{'type': 'line', 'value': 'asdf'},
                          {'type': 'section', 'level': 1, 'title': 'xyz',
                           'children': []}],
                         parse_markdown_lines(["asdf", "# xyz"]))

        tree = parse_markdown_lines(["# a", "0", "## b", "1", "# c", "2"])
        self.assertEqual([{'type': 'section',
                           'level': 1,
                           'title': 'a',
                           'children':
                           [{'type': 'line', 'value': '0'},
                            {'type': 'section',
                             'level': 2,
                             'title': 'b',
                             'children':
                             [{'type': 'line', 'value': '1'}]}]},
                          {'type': 'section',
                           'level': 1,
                           'title': 'c',
                           'children': [{'type': 'line', 'value': '2'}]}],
                         tree)

        paths = []
        def visitor(path, element):
            paths.append(path)
            return element
        tree2 = postwalk_markdown_tree(tree, [], visitor)
        self.assertEqual(tree2, tree)
        self.assertEqual([['a'], ['a', 'b'], ['a'], [], ['c'], []], paths)

        def roundtrip(lines):
            self.assertEqual(lines, render_markdown(parse_markdown_lines(lines)))

        roundtrip(["asdf"])
        roundtrip(["# a", "0", "## b", "1", "# c", "2"])
        roundtrip(["## a", "0", "## b", "1"])
        roundtrip(["xyz", "###### a", "0", "#### b", "1", "########### c", "3"])

        p0 = parse_markdown_lines(["# A", "xyz", "## B", "123", "# C", "pqr"])
        p1 = edit_section(p0, lambda x: x == ["A", "B"],
                          lambda section: {**section, "title": "Hej"})
        self.assertEqual(['# A', 'xyz', '## Hej', '123', '# C', 'pqr'],
                         render_markdown(p1))

        m = section_map(parse_markdown_lines(["# A", "## B"]))
        self.assertEqual("A", m[("A",)]["title"])
        self.assertEqual("B", m[("A", "B",)]["title"])

        
    
        
def run_tests():
    unittest.main(argv=[""], exit=False)

parser = argparse.ArgumentParser(prog="dlogtool",
                                 description="Decision log tool")
parser.add_argument("--path", type=Path, default=Path())
parser.add_argument("--team", type=str, help="Name of the team that made the decision",
                    default="")
subparsers = parser.add_subparsers(help="Subcommands", dest="subcommand")
new_parser = subparsers.add_parser("new", help="Create a new decision entry")
new_parser.add_argument("title", type=str, help="Title of the log entry")
subparsers.add_parser("rebuild", help="Rebuild the repository")
subparsers.add_parser("test", help="Run the tests")

def main(args):
    pargs = parser.parse_args(args)
    if pargs.subcommand == "rebuild":
        rebuild_repo(pargs.path)
    elif pargs.subcommand == "new":
        full_path = new_entry(pargs.path, pargs.title, pargs.team)
        rebuild_repo(pargs.path)
        print(full_path.name)
    elif pargs.subcommand == "test":
        run_tests()
    
if __name__ == "__main__":
    main(sys.argv[1:])

if False:
    run_tests()
