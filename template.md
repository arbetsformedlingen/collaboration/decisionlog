# Title
Title of the decision.

# Team
Name of the team that made the decision.

# Context
The context should explain the problem or situation that led to the decision. This section should be detailed enough for someone new to the project to understand the problem being solved.

# Decision
The decision section should clearly explain what decision was made and why. This should include any options that were considered and why they were not chosen. It's important to include enough information for others to understand the reasoning behind the decision.

# Status
The status section should explain the current state of the decision. This should include any implementation details and timelines.

# Consequences
The consequences section should list any potential positive or negative consequences of the decision. This should include any risks or tradeoffs that were considered.
