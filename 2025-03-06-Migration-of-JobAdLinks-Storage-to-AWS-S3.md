# Title
Migration of JobAdLinks Storage to AWS S3

# Team
OpenData

# Context
The project required a migration from on-premise servers to a more efficient lightweight runner environment. As part of this migration, we identified the need to transition from the existing on-prem Minio storage backend to AWS S3. This transition was necessary because Minio is being shut down, and we need a storage solution which is reachable from our lightweight runner environment.

# Decision
In the JobAdLinks migration from onprem servers to a lightweight runner environment externally, we also need to switch from the onprem Minio storage backend to AWS S3. We have decided to create two buckets: one acting as a short term technical cache which is autocleaned of files older than two days, and one long term storage bucket for files that we need to archive for longer times. These include the outputfiles that have been stripped of original descriptions, and also for backup of the sqlite database files.

# Status
The decision has been made to create two S3 buckets: one for short-term cache (autocleaning files older than two days) and another for long-term storage of essential files. Implementation is underway to set up these buckets as part of the migration process.

# Consequences
Positive consequences include improved storage scalability and reliability through AWS S3. Negative consequences may involve added complexity in managing two distinct buckets. The files stored in the buckets will not contain sensitive information that has not already been publicly published on the Internet.
