## Title
Decisions logged to files in a repo.

## Context
The lack of traceability is a problem in any organisation including
JobTech Dev. With a logging system things may improve, so we have
decided to start. This file consitutes the founding decision and start
of the initiative.

## Decision
We, Jonas Södergren, Joakim Verona, Oleg Lyalin and Per Weijnitz, will
start to log decisions in this repo.

## Status
agreed

## Consequences
 + Traceability and accountability may improve
 - More red tape work
